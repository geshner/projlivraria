unit CONEXAO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TDMCONEXAO = class(TDataModule)
    Database: TDatabase;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMCONEXAO: TDMCONEXAO;

implementation

{$R *.DFM}

procedure TDMCONEXAO.DataModuleCreate(Sender: TObject);
begin
   Database.Connected := true;
end;

end.
