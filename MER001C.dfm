inherited FMER001C: TFMER001C
  Left = 438
  Top = 296
  Caption = 'Cadastro de Editoras'
  ClientHeight = 322
  ClientWidth = 503
  ExplicitLeft = 438
  ExplicitTop = 296
  ExplicitWidth = 519
  ExplicitHeight = 360
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = -5
    Top = 0
    Width = 510
    Height = 273
    TabOrder = 0
    object Image1: TImage
      Left = 179
      Top = 41
      Width = 20
      Height = 20
      AutoSize = True
      Center = True
      Enabled = False
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000140000
        001408060000008D891D0D000000017352474200AECE1CE90000000467414D41
        0000B18F0BFC61050000001974455874536F6674776172650041646F62652049
        6D616765526561647971C9653C000000F14944415478DAAD94811182300C45DB
        0D740436800D740436D0119C40477003DD4037B04EA0237403D840130C1A42C2
        B587FFEEDF41298F9FA487777F9637D637E093B21EC115B8CD01BE12825CC175
        0A50C202F84ED77BF1EC49694D2087590916E086DDD7B47704E43D33CB313EEE
        35A0B9C1100F5051F92A30259D9B7A4702D7EE33881C6004171670D4E404E060
        DA121828650EF00CDE5A40BE36A583FB9DCB82CA1EBCBC02DFB41214C9B3A81E
        1B99323AD668A612FC106BEAB1D1A0A8967A54521596BE50AD5F0D9595AB0E6A
        0D00D35C1430A6ED27AAFD95962913B52487830A73801AF43817D843B13D3894
        DD1B44FF3B73B52CAC930000000049454E44AE426082}
    end
    object Label1: TLabel
      Left = 64
      Top = 48
      Width = 37
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 64
      Top = 96
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label3: TLabel
      Left = 64
      Top = 160
      Width = 49
      Height = 13
      Caption = 'Endere'#231'o:'
    end
    object Label4: TLabel
      Left = 64
      Top = 216
      Width = 46
      Height = 13
      Caption = 'Telefone:'
    end
    object btnSearchEdt: TSpeedButton
      Left = 177
      Top = 39
      Width = 25
      Height = 25
    end
    object edCodEdt: TEdit
      Left = 112
      Top = 45
      Width = 50
      Height = 21
      TabOrder = 0
      OnEnter = edCodEdtEnter
      OnExit = edCodEdtExit
      OnKeyPress = edCodEdtKeyPress
    end
    object edNomeEdt: TEdit
      Left = 64
      Top = 115
      Width = 300
      Height = 21
      TabOrder = 1
    end
    object edEnderecoEdt: TEdit
      Left = 64
      Top = 179
      Width = 300
      Height = 21
      TabOrder = 2
    end
    object edFoneEdt: TEdit
      Left = 64
      Top = 235
      Width = 150
      Height = 21
      TabOrder = 3
    end
  end
  object btnExcluirEdt: TButton
    Left = 174
    Top = 287
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 1
    OnClick = btnExcluirEdtClick
  end
  object btnCadastraEdt: TButton
    Left = 255
    Top = 287
    Width = 75
    Height = 25
    Caption = 'Cadastrar'
    TabOrder = 2
    OnClick = btnCadastraEdtClick
  end
  object btnLimpaEdt: TButton
    Left = 336
    Top = 287
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 3
    OnClick = btnLimpaEdtClick
  end
  object btnCancelEdt: TButton
    Left = 417
    Top = 287
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancelar'
    TabOrder = 4
    OnClick = btnCancelEdtClick
  end
  object BalloonHint1: TBalloonHint
    Left = 248
    Top = 32
  end
  object qyCadEdt: TQuery
    DatabaseName = 'LIVRO'
    Left = 304
    Top = 32
  end
end
