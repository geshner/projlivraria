unit MER001C;
{
 Unit de gerenciamento de editoras:
   - Cadastro;
   - Atualiza��o;
   - Exclus�o;

 Criado em: 16/12/2014
 Criado por: Geshner.
}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, PAIMDICHILD, Vcl.StdCtrls, Data.DB, Bde.DBTables,
  FUNCOES, Vcl.Imaging.pngimage, Vcl.ExtCtrls, Vcl.Buttons;

type
  TFMER001C = class(TFPAIMDICHILD)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edCodEdt: TEdit;
    Label2: TLabel;
    edNomeEdt: TEdit;
    Label3: TLabel;
    edEnderecoEdt: TEdit;
    Label4: TLabel;
    edFoneEdt: TEdit;
    BalloonHint1: TBalloonHint;
    qyCadEdt: TQuery;
    btnExcluirEdt: TButton;
    btnCadastraEdt: TButton;
    btnLimpaEdt: TButton;
    btnCancelEdt: TButton;
    btnSearchEdt: TSpeedButton;
    Image1: TImage;

    procedure limpaCampos();
    procedure edCodEdtEnter(Sender: TObject);
    procedure edCodEdtExit(Sender: TObject);
    procedure edCodEdtKeyPress(Sender: TObject; var Key: Char);
    procedure btnLimpaEdtClick(Sender: TObject);
    procedure btnCancelEdtClick(Sender: TObject);
    procedure btnExcluirEdtClick(Sender: TObject);
    procedure btnCadastraEdtClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMER001C: TFMER001C;
  tabela : String = 'EDITORA';

implementation

{$R *.dfm}

{ TFMER001C }

procedure TFMER001C.btnCadastraEdtClick(Sender: TObject);
begin
  inherited;
   if (Trim(edNomeEdt.Text) <> '') AND
      (Trim(edEnderecoEdt.Text) <>'') AND
      (Trim(edFoneEdt.Text) <> '') then
   begin

      limpaQuery(qyCadEdt);

      qyCadEdt.SQL.Text := 'SELECT ID_EDT FROM EDITORA WHERE ID_EDT = ' + edCodEdt.Text;
      qyCadEdt.Open;

      if  qyCadEdt.IsEmpty then
      begin
         limpaQuery(qyCadEdt);
         qyCadEdt.SQL.Add('INSERT INTO EDITORA(ID_EDT, NOME_EDT, ENDERECO_EDT, FONE_EDT)' +
                                      'VALUES (:ID_EDT, :NOME_EDT, :ENDERECO_EDT, :FONE_EDT)');
      end
      else
      begin
         limpaQuery(qyCadEdt);
         qyCadEdt.SQL.Add('UPDATE EDITORA SET NOME_EDT = :NOME_EDT, ENDERECO_EDT = :ENDERECO_EDT, ' +
                          'FONE_EDT = :FONE_EDT WHERE ID_EDT = :ID_EDT');
      end;

      qyCadEdt.ParamByName('ID_EDT').AsInteger := StrToInt(edCodEdt.Text);
      qyCadEdt.ParamByName('NOME_EDT').AsString := edNomeEdt.Text;
      qyCadEdt.ParamByName('ENDERECO_EDT').AsString := edEnderecoEdt.Text;
      qyCadEdt.ParamByName('FONE_EDT').AsString := edFoneEdt.Text;
      qyCadEdt.ExecSQL;
      if  qyCadEdt.RowsAffected <> 1 then
         ShowMessage('Erro ao cadastrar a editora!')
      else
      begin
         ShowMessage('Editora Cadastrada com sucesso!');
         limpaCampos();
      end;
   end
   else
      ShowMessage('Preencha todos os campos!');
end;

procedure TFMER001C.btnCancelEdtClick(Sender: TObject);
begin
  inherited;
   Close;
end;

procedure TFMER001C.btnExcluirEdtClick(Sender: TObject);
begin
  inherited;
   limpaQuery(qyCadEdt);

   qyCadEdt.SQL.Add('DELETE FROM EDITORA WHERE ID_EDT= :ID_EDT');
   qyCadEdt.ParamByName('ID_EDT').AsInteger := StrToInt(edCodEdt.Text);

   qyCadEdt.ExecSQL;

   limpaQuery(qyCadEdt);

   limpaCampos();

   ShowMessage('Cliente excluido com sucesso!');
end;

procedure TFMER001C.btnLimpaEdtClick(Sender: TObject);
begin
  inherited;
   limpaCampos();
end;

procedure TFMER001C.edCodEdtEnter(Sender: TObject);
begin
  inherited;
  limpaCampos();
  edCodEdt.Text := proximaChave(tabela, 'ID_EDT');

end;

procedure TFMER001C.edCodEdtExit(Sender: TObject);
var
   id : Integer;
begin
  inherited;

  if Trim(edCodEdt.Text) <> '' then
  begin
      id := StrToInt(edCodEdt.Text);

      limpaQuery(qyCadEdt);

      qyCadEdt.SQL.Add('SELECT * FROM EDITORA WHERE ID_EDT = :ID_EDT');
      qyCadEdt.ParamByName('ID_EDT').AsInteger := id;
      qyCadEdt.Open;
  end;

  if not qyCadEdt.IsEmpty then
  begin
      edNomeEdt.Text := qyCadEdt.FieldByName('NOME_EDT').AsString;
      edEnderecoEdt.Text := qyCadEdt.FieldByName('ENDERECO_EDT').AsString;
      edFoneEdt.Text := qyCadEdt.FieldByName('FONE_EDT').AsString;
  end
  else
      edCodEdt.Text := proximaChave(tabela, 'ID_EDT');

  limpaQuery(qyCadEdt);

end;

procedure TFMER001C.edCodEdtKeyPress(Sender: TObject; var Key: Char);
var
   R : TRect;
begin
   inherited;

   BalloonHint1.HideHint;
   if not somenteNumero(Key) then
   begin
      R := edCodEdt.BoundsRect;
      R.TopLeft := ClientToScreen(R.TopLeft);
      R.BottomRight := ClientToScreen(R.BottomRight);
      BalloonHint1.Description := 'Somente n�meros!';
      BalloonHint1.HideAfter := 2000;
      BalloonHint1.Delay := 0;
      BalloonHint1.ShowHint(R);
      Key := #0;
   end;

end;

procedure TFMER001C.limpaCampos;
begin
   edCodEdt.Clear;
   edNomeEdt.Clear;
   edEnderecoEdt.Clear;
   edFoneEdt.Clear;
   edCodEdt.SetFocus;
end;

end.
