unit PAIPRINCIPAL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, CONEXAO, FUNCOES, MER000C, MER001C;

type
  TFPAIPRINCIPAL = class(TForm)
    MainMenu1: TMainMenu;
    Cadastro1: TMenuItem;
    CLiente1: TMenuItem;
    Editora1: TMenuItem;
    procedure CLiente1Click(Sender: TObject);
    procedure Editora1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPAIPRINCIPAL: TFPAIPRINCIPAL;


implementation

{$R *.dfm}



procedure TFPAIPRINCIPAL.CLiente1Click(Sender: TObject);
begin
   if not existeForm('FMER000C') then
   begin
      Application.CreateForm(TFMER000C, FMER000C );
   end;
end;

procedure TFPAIPRINCIPAL.Editora1Click(Sender: TObject);
begin
   if not existeForm('FMER001C') then
   begin
      Application.CreateForm(TFMER001C, FMER001C );
   end;
end;

end.
