unit FUNCOES;

interface

uses
   Vcl.Controls,Vcl.Forms, Vcl.StdCtrls, System.SysUtils, System.Types, Data.DB, Bde.DBTables;

function existeForm(form : String) : boolean;
function somenteNumero(tecla : Char) : boolean;
function proximaChave(tabela : String; chave : String) : String;

procedure limpaQuery(Q : TQuery);
implementation

function existeForm(form : String) : boolean;
var
   i : integer;
begin
   Result := false;
   for i := 0 to Screen.FormCount - 1 do
   begin
      if UpperCase(Screen.Forms[i].name) = UpperCase(form) then
      begin
         Result := true;
         break;
      end
   end;

end;

function somenteNumero(tecla : Char) : boolean;
begin
   Result :=  (CharInSet(tecla, ['0'..'9', #8, #9, #13]));
end;

function proximaChave(tabela : String; chave : String) : String;
var
   Q : TQuery;
   n : String;
begin
   Q := TQuery.Create(Application);
   Q.DatabaseName := 'LIVRO';
   Q.SQL.Add('SELECT (MAX('+chave+')+1) AS COD FROM ' + tabela);
   Q.Open;
   n := Q.FieldByName('COD').AsString;
   Q.SQL.Clear;
   Q.Close;
   FreeAndNil(Q);

   Result := n;
end;

procedure limpaQuery(Q : TQuery);
begin
   Q.SQL.Clear;
   Q.Close;
end;

end.
