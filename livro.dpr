program livro;

uses
  Vcl.Forms,
  PAIPRINCIPAL in 'PAIPRINCIPAL.pas' {FPAIPRINCIPAL},
  PAIMDICHILD in 'PAIMDICHILD.pas' {FPAIMDICHILD},
  MER000C in 'MER000C.pas' {FMER000C},
  CONEXAO in 'CONEXAO.pas' {DMCONEXAO: TDataModule},
  FUNCOES in 'FUNCOES.pas',
  MER001C in 'MER001C.pas' {FMER001C};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDMCONEXAO, DMCONEXAO);
  Application.CreateForm(TFPAIPRINCIPAL, FPAIPRINCIPAL);
  Application.Run;
end.
